package test.twitter.vivekbansal.vttest;

import android.app.Activity;

import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.UIKitCustomSetting;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.scancard.ScanCard;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import java.util.ArrayList;

/**
 * Created by vivekbansal on 25/10/16.
 */
public class VeritransManager implements TransactionFinishedCallback {

    private final String BILLING_ADDRESS_CITY = "Jakarta";
    private final String BILLING_ADDRESS_COUNTRY_CODE = "IDN";

    private Activity mActivity;
    private MidtransSDK midtransSDK;
    private TransactionFinishedCallback mTransactionFinishedCallback;


    public VeritransManager(Activity activity, TransactionFinishedCallback transactionFinishedCallback) {
        mActivity = activity;
        mTransactionFinishedCallback = transactionFinishedCallback;

        initializeVeritransSDK();
    }

    private void initializeVeritransSDK() {

        UIKitCustomSetting uisetting = new UIKitCustomSetting();
        uisetting.setShowPaymentStatus(false);

        SdkUIFlowBuilder.init(mActivity, BuildConfig.VERITRANS_CLIENT_KEY, BuildConfig.VERITRANS_BASE_URL, this)
                .setExternalScanner(new ScanCard())
                .enableLog(true)
                .setDefaultText("fonts/Roboto-Regular.ttf")
                .setSemiBoldText("fonts/Roboto-Medium.ttf")
                .useBuiltInTokenStorage(false)
                .setBoldText("fonts/Roboto-Bold.ttf")
                .setUIkitCustomSetting(uisetting)
                .buildSDK();

        midtransSDK = MidtransSDK.getInstance();
        midtransSDK.setDefaultText("fonts/Roboto-Regular.ttf");
        midtransSDK.setSemiBoldText("fonts/Roboto-Medium.ttf");
        midtransSDK.setBoldText("fonts/Roboto-Bold.ttf");
    }

    public void createTransactionRequest(String orderId) {


        // Set user details
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName("Vivek Bansal");
        userDetail.setEmail("vivekbansal001@gmail.com");
        userDetail.setPhoneNumber("22222222");
        userDetail.setUserId("210");

        // Initiate address list
        ArrayList<UserAddress> userAddresses = new ArrayList<>();

        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("");
        userAddress.setCity(BILLING_ADDRESS_CITY);
        userAddress.setCountry(BILLING_ADDRESS_COUNTRY_CODE);
        userAddress.setZipcode("");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);

        // Set user address to user detail object
        userDetail.setUserAddresses(userAddresses);

        // Save the user detail. It will skip the user detail screen
        LocalDataHandler.saveObject("user_details", userDetail);


        // Create array list and add above item details in it and then set it to transaction request.
        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
        ItemDetails itemDetails = new ItemDetails(orderId, 110000, 1, "3 Bulan(iFlix)");
        itemDetailsList.add(itemDetails);


        TransactionRequest transactionRequest = new TransactionRequest(orderId, 110000);

        CreditCard creditCardOptions = new CreditCard();
        // Set to true if you want to save card to Snap
        creditCardOptions.setSaveCard(true);
        // Set to true to save card token as `one click` token
        creditCardOptions.setSecure(true);
        transactionRequest.setCreditCard(creditCardOptions);

        // Set card payment info
        transactionRequest.setCardPaymentInfo(mActivity.getString(R.string.card_click_type_two_click), true);
        // Set item details into the transaction request.
        transactionRequest.setItemDetails(itemDetailsList);

        midtransSDK.setTransactionRequest(transactionRequest);
    }

    public void startCardPayment() {
        midtransSDK.startPaymentUiFlow(mActivity);
    }

    @Override
    public void onTransactionFinished(TransactionResult transactionResult) {
        mTransactionFinishedCallback.onTransactionFinished(transactionResult);
    }

}
