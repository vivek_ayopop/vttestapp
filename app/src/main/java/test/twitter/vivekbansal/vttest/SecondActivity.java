package test.twitter.vivekbansal.vttest;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

public class SecondActivity extends AppCompatActivity implements TransactionFinishedCallback {

    VeritransManager mVeritransManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onVTPaymentClicked(View view) {
        EditText editText = (EditText) findViewById(R.id.et_order_id);

        mVeritransManager = new VeritransManager(this, this);
        mVeritransManager.createTransactionRequest(editText.getText().toString());
        mVeritransManager.startCardPayment();

    }

    @Override
    public void onTransactionFinished(TransactionResult transactionResult) {
        ProgressDialog progressDialog = ProgressDialog.show(this, "", "Loading");
        progressDialog.setCancelable(true);
    }
}
